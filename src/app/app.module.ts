
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ArmarioGalleryComponent } from './components/armario-gallery/armario-gallery.component';
import { ArmarioHeaderComponent } from './components/armario-header/armario-header.component';
import { NewPrendaComponent } from './components/new-prenda/new-prenda.component';
import { ArmarioService } from './services/armario.service';
import { ArmarioDetailComponent } from './components/armario-gallery/armario-detail/armario-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    ArmarioGalleryComponent,
    ArmarioHeaderComponent,
    NewPrendaComponent,
    ArmarioDetailComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, ReactiveFormsModule],
  providers: [ArmarioService],
  bootstrap: [AppComponent]
})
export class AppModule { }
