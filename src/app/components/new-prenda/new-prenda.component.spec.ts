import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPrendaComponent } from './new-prenda.component';

describe('NewPrendaComponent', () => {
  let component: NewPrendaComponent;
  let fixture: ComponentFixture<NewPrendaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewPrendaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPrendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
