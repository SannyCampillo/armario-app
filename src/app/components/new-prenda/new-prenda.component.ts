import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ArmarioService } from './../../services/armario.service';

import { Iprenda } from './../models/Iprenda';
import { Icategory } from './models/Icategory';

@Component({
  selector: 'app-new-prenda',
  templateUrl: './new-prenda.component.html',
  styleUrls: ['./new-prenda.component.scss']
})
export class NewPrendaComponent implements OnInit {
public prendaForm: FormGroup | null = null;
// tslint:disable-next-line: no-inferrable-types
public submitted: boolean = false;
public categoryList = [];

constructor(
  private formBuilder: FormBuilder,
  private armarioService: ArmarioService
  ) {
  this.prendaForm = this.formBuilder.group({
    name: ['', [Validators.required, Validators.maxLength(20)]],
    category: ['', [Validators.required, Validators.maxLength(20)]],
    quantity: ['', [Validators.required, Validators.maxLength(20)]],
    photo: ['', [Validators.required, Validators.minLength(2)]],
  });
  this.categoryList = this.getcategoryList();
 }

  ngOnInit(): void {}

  public onSubmit(): void {
  this.submitted = true;
  if (this.prendaForm.valid) {
    const prenda: Iprenda = {
      name: this.prendaForm.get('name').value,
      category: this.prendaForm.get('category').value,
      quantity: this.prendaForm.get('quantity').value,
      photo: this.prendaForm.get('photo').value,
    };

    this.armarioService.postArmario(prenda);

    this.prendaForm.reset();
    this.submitted = false;
  }
}
  getcategoryList(): Icategory [] {
  return [
    {
      id: '1',
      name: 'Pantalones'
    },
    {
      id: '2',
      name: 'Vestidos'
    },
    {
      id: '3',
      name: 'Faldas'
    },
    {
      id: '4',
      name: 'Sudaderas'
    },
    {
      id: '5',
      name: 'Camisetas'
    }
  ];
  }
}
