import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-armario-header',
  templateUrl: './armario-header.component.html',
  styleUrls: ['./armario-header.component.scss']
})
export class ArmarioHeaderComponent implements OnInit {

  logo: string;
  logoAlt: string;
  home: string;
  miarmario: string;
  about: string;
  contact: string;

  constructor() {
    this.logo = 'https://www.thefashionroom.net/wp-content/uploads/2015/11/asesoria-de-armario.png';
    this.logoAlt = 'Blog logo';
    this.home = 'HOME';
    this.miarmario = 'MI ARMARIO';
    this.about = 'ABOUT';
    this.contact = 'CONTACT';
  }

  ngOnInit(): void {
  }
}
