import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArmarioHeaderComponent } from './armario-header.component';

describe('ArmarioHeaderComponent', () => {
  let component: ArmarioHeaderComponent;
  let fixture: ComponentFixture<ArmarioHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArmarioHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArmarioHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
