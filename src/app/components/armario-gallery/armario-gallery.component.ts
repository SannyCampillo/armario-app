import { Iprenda } from './../models/Iprenda';
import { Component, OnInit } from '@angular/core';
import { ArmarioService } from 'src/app/services/armario.service';

@Component({
  selector: 'app-armario-gallery',
  templateUrl: './armario-gallery.component.html',
  styleUrls: ['./armario-gallery.component.scss']
})
export class ArmarioGalleryComponent implements OnInit {
  public myArmario: Iprenda[] | null = null;
  public namePrenda: any | null = null;
  constructor(private armarioService: ArmarioService) {}

  ngOnInit(): void {
    this.myArmario = this.armarioService.getArmario();
    this.namePrenda = this.armarioService.getArmario();
    console.log(this.namePrenda);
  }

}
