import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArmarioGalleryComponent } from './armario-gallery.component';

describe('ArmarioGalleryComponent', () => {
  let component: ArmarioGalleryComponent;
  let fixture: ComponentFixture<ArmarioGalleryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArmarioGalleryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArmarioGalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
