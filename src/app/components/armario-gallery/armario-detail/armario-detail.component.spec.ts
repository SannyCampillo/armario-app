import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArmarioDetailComponent } from './armario-detail.component';

describe('ArmarioDetailComponent', () => {
  let component: ArmarioDetailComponent;
  let fixture: ComponentFixture<ArmarioDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArmarioDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArmarioDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
