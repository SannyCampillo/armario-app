import { Iprenda } from './../../models/Iprenda';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-armario-detail',
  templateUrl: './armario-detail.component.html',
  styleUrls: ['./armario-detail.component.scss']
})
export class ArmarioDetailComponent implements OnInit {
  @Input() Armario: Iprenda;

  @Output() deleteArmario = new EventEmitter<boolean>();

  constructor() {}


  ngOnInit(): void {}

}
