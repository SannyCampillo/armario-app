export interface Iprenda {
    name: string;
    category: string[];
    quantity: number;
    photo: string;
}
