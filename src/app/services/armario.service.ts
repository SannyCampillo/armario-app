
import { Iprenda } from './../components/models/Iprenda';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ArmarioService {
private Armario: Iprenda [];
  constructor() {
    this.Armario = [
      {
        name: 'Pantalón vaquero',
        category: ['Pantalones'],
        quantity: 1,
        photo: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQEuk7Do1k4zNcpJtytH07EBbsbbSWQKQLyxw&usqp=CAU',
      }
    ];

   }
   getArmario(): Iprenda[] {
    return this.Armario;
  }

  postArmario(Armario: Iprenda): void {
    this.Armario.push(Armario);
  }

  deleteArmario(): boolean {
  this.deleteArmario.emit(false);
  }

 }

